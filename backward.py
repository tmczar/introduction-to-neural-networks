#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Example(object):
    def __init__(self, arm_length=0.6):
        self.arm_length = arm_length
        self.center = Point(0, 0)
        self.input = []
        self.output = []

    def generate(self, no_of_examples):
        self.input = []
        self.output = []
        for i in range(no_of_examples):
            point = self.generate_point()
            self.input.append([point.x, point.y])
        return self.input

    def generate_point(self):
        alpha = np.random.random() * np.pi
        beta = np.random.random() * np.pi
        self.output.append([alpha / np.pi, beta / np.pi])
        temp_point = self.translate(self.center, alpha)
        final_point = self.translate(temp_point, np.pi - beta + alpha)
        #final_point = self.translate(temp_point, beta)
        #final_point = self.translate(temp_point, beta + alpha - np.pi)
        return final_point

    def toPoints(self, res):
        alpha = res[0] * np.pi
        beta = res[1] * np.pi
        first = self.translate(self.center, alpha)
        last = self.translate(first, np.pi - beta + alpha)
        #last = self.translate(first, beta)
        #last = self.translate(first, beta + alpha - np.pi)
        return (first, last)

    def translate(self, center, angle):
        return Point(center.x + self.arm_length * np.sin(angle), center.y - self.arm_length * np.cos(angle))


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_derivative(x):
    return sigmoid(x) * (1 - sigmoid(x))

class Neuron(object):
    def __init__(self, no_of_inputs, eta=0.1):
        self.no_of_inputs = no_of_inputs
        self.eta = eta
        self.weights = np.random.random(self.no_of_inputs) - 0.5
        self.activation = 0
        self.sigma = 0
        self.delta = 0

    @staticmethod
    def sigmoid(x):
        return 1 / (1 + np.exp(-x))

    def sigmoid_derivative(self):
        return self.sigma * (1 - self.sigma)

    def output(self, x):
        activation = 0
        for i in range(self.no_of_inputs):
            activation += self.weights[i] * x[i]
        self.sigma = self.sigmoid(activation)
        return self.sigma.copy()

    def update_weights(self, x):
        for i in range(self.no_of_inputs):
            self.weights[i] -= self.eta * self.delta * x[i]


class Layer(object):
    def __init__(self, layer_size, prev_layer_size, eta=0.1):
        self.layer_size = layer_size
        self.prev_layer_size = prev_layer_size
        self.eta = eta
        self.neurons = []
        self.input = []
        for i in range(self.layer_size):
            self.neurons.append(Neuron(self.prev_layer_size, eta))

    def output(self, x):
        result = []
        for i in range(self.layer_size):
            result.append(self.neurons[i].output(x))
        return np.array(result)

    def update_weights(self, x):
        for i in range(self.layer_size):
            self.neurons[i].update_weights(x)


class NeuralNetwork(object):
    def __init__(self, structure, iterations=100, eta=0.1):
        self.structure = structure
        self.iterations = iterations
        self.eta = eta
        self.layers = []
        self.errors = []
        self.output = []
        for i in range(len(self.structure) - 1):
            self.layers.append(Layer(self.structure[i + 1], self.structure[i], eta))

    def train(self, training_data_x, training_data_y):
        for i in range(self.iterations * len(training_data_x)):
            index = np.random.randint(len(training_data_x))
            inp = np.array(training_data_x[index]) / 20
            output = np.array(training_data_y[index]) / 4

            self.forward(inp)
            self.backward(output)
            #print(inp, output, self.output)

    def forward(self, input):
        self.layers[0].input = input
        for i in range(len(self.structure) - 2):
            inp = self.layers[i].output(self.layers[i].input)
            self.layers[i + 1].input = inp.copy()
        self.output = self.layers[i + 1].output(self.layers[i + 1].input).copy()
        return self.output

    def backward(self, output):
        last_layer = len(self.layers) - 1
        for j in range(self.layers[last_layer].layer_size):
            epsilon = output[j] - self.output[j]
            self.layers[last_layer].neurons[j].delta = epsilon * self.layers[last_layer].neurons[j].sigmoid_derivative()
        self.layers[last_layer].update_weights(self.layers[last_layer].input)

        for l in reversed(range(len(self.layers) - 1)):
            for j in range(self.layers[l].layer_size):
                epsilon = 0
                for k in range(self.layers[l + 1].layer_size):
                    epsilon += self.layers[l + 1].neurons[k].weights[j] * self.layers[l + 1].neurons[k].delta
                self.layers[l].neurons[j].delta = epsilon * self.layers[l].neurons[j].sigmoid_derivative()
            self.layers[l].update_weights(self.layers[l].input)


class NPNeural_Network(object):
    def __init__(self, eta=0.02):
        self.input_size = 2
        self.hidden_size = 10
        self.output_size = 2
        self.eta = eta
        self.W1 = np.random.randn(self.input_size, self.hidden_size)
        self.W2 = np.random.randn(self.hidden_size, self.output_size)
        self.errors = []
        self.errors2x = []
        self.errors2y = []
        self.errors2z = []

    @staticmethod
    def sigmoid(x):
        return 1 / (1 + np.exp(-x))

    @staticmethod
    def sigmoid_derivative(s):
        return s * (1 - s)

    def forward(self, x):
        self.z = np.dot(x, self.W1)
        self.z2 = self.sigmoid(self.z)
        self.z3 = np.dot(self.z2, self.W2)
        o = self.sigmoid(self.z3)
        return o

    def backward(self, x, y, o):
        self.o_error = (y - o)*1
        self.o_delta = self.o_error*self.sigmoid_derivative(o)

        self.z2_error = self.o_delta.dot(self.W2.T)
        self.z2_delta = self.z2_error*self.sigmoid_derivative(self.z2)

        self.W1 += x.T.dot(self.z2_delta)
        self.W2 += self.z2.T.dot(self.o_delta)

    def train(self, x, y):
        self.errors2x = []
        self.errors2y = []
        self.errors2z = []
        o = self.forward(x)
        for a, b, c in zip(x, y, o):
            z_pos = np.sqrt((b[0] - c[0])**2 + (b[1] - c[1])**2)
            x_pos = a[0]
            y_pos = a[1]
            self.errors2x.append(x_pos)
            self.errors2y.append(y_pos)
            self.errors2z.append(z_pos)
        self.backward(x, y, o)
        self.errors.append(np.mean(np.square(y - o)))

from mpl_toolkits.mplot3d import axes3d
# struc = [2, 10, 10, 3]
# nn = NeuralNetwork(struc)
# nn.forward(np.random.random(2))
# nn.backward(np.random.random(3))
# print(nn.output)

example = Example()
struc = [2, 10, 10, 2]
#nn = NeuralNetwork(struc)
example.generate(150)
nn = NPNeural_Network()
#for _ in range(10000):
#    index = np.random.randint(len(example.input)-50)
#    nn.train(np.array(example.input[index: index+50]), np.array(example.output[index: index+50]))
ii = 100
for _ in range(1000):
    nn.train(np.array(example.input), np.array(example.output))
    ii += 1
    if ii >= 100:
        ii -= 100
        fig = plt.figure()
        ax = plt.axes(projection="3d")
        surf = ax.plot_trisurf(nn.errors2x, nn.errors2y, nn.errors2z, cmap='viridis')
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.show()

res = nn.forward(example.input[0])
pts = example.toPoints(res)
coords = [pts[0].x, pts[0].y, pts[1].x, pts[1].y]
print(res, coords)
print(example.output[0], example.input[0])

#print(example.input)
#exit(0)




import matplotlib as mpl

mpl.pyplot.plot(nn.errors)
mpl.pyplot.show()

import pygame

pygame.init()
clock = pygame.time.Clock()
DGRAY = (20, 20, 20)
GRAY = (80, 80, 80)
GREEN = (0, 255, 0)
BLUE = (106, 159, 181)
WHITE = (255, 255, 255)
PINK = (255, 200, 200)

display_surface = pygame.display.set_mode((1000, 800))
w, h = pygame.display.get_surface().get_size()

display_surface.fill(DGRAY)

zero = (300, h//2)
pos = zero
p1 = zero
p2 = zero
follow = False
counter = 0

def toScreen(point, zero):
    coords = [point[0] * 150 + zero[0], -point[1] * 150 + zero[1]]
    return coords

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            follow = not follow
    if follow:
        pos = pygame.mouse.get_pos()
        x = (pos[0] - zero[0]) / 150
        y = -(pos[1] - zero[1]) / 150
        res = nn.forward((x, y))
        pts = example.toPoints(res)
        coords = np.array([pts[0].x, pts[0].y, pts[1].x, pts[1].y]) * 150
        p1 = (coords[0] + zero[0], - coords[1] + zero[1])
        p2 = (coords[2] + zero[0], - coords[3] + zero[1])

        counter += 1
        if counter > 100:
            print(x, y, res)
            #print(pos)
            print(p1, p2)
            counter %= 100

    display_surface.fill(DGRAY)
    pygame.draw.circle(display_surface, BLUE, zero, 7)
    pygame.draw.circle(display_surface, GREEN, pos, 7)
    pygame.draw.line(display_surface, PINK, zero, p1, 2)
    pygame.draw.circle(display_surface, PINK, p1, 5)
    pygame.draw.line(display_surface, WHITE, p1, p2, 2)
    pygame.draw.circle(display_surface, WHITE, p2, 5)
    for ex in example.input:
        pygame.draw.circle(display_surface, GRAY, toScreen(ex, zero), 1)
    pygame.display.update()
    clock.tick(30)
