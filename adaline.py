#!/usr/bin/env python3
import random

import numpy as np
import matplotlib.pyplot as plot
from defines import noise

def fourier_transform(x):
    a = np.abs(np.fft.fft(x))
    a[0] = 0
    if np.max(a) == 0:
        return a
    else:
        return a/np.max(a)


def sigmoid(x):
    return 1/(1 + np.exp(x))


def sigmoid_derivative(x):
    return sigmoid(x) * (1 - sigmoid(x))

class Adaline(object):
    def __init__(self, no_of_inputs, name = 'name', learning_rate = 0.01, iterations = 500, biased = True):
        self.no_of_inputs = no_of_inputs
        self.learning_rate = learning_rate
        self.iterations = iterations
        self.name = name
        self.biased = biased
        self.weights = np.random.random(2*self.no_of_inputs + 1) - 0.5
        self.errors = []
    
    def standarize(self, x):
        return (x - np.mean(x)) / np.std(x)
    
    def train(self, training_data_x, training_data_y):
        for _ in range(self.iterations):
            err = 0
            for __ in range(len(training_data_x)):
                #std_training_data_x = self.standarize(training_data_x)
                std_training_data_x = training_data_x
                index = random.randrange(len(training_data_x))
                x = std_training_data_x[index]
                y = training_data_y[index]
                noised_x = noise(x, 5)
                x_ft = np.concatenate([noised_x, fourier_transform(noised_x)])
                out = self.output(noised_x)
                self.weights[1:] += self.learning_rate * (y - out) * x_ft * self.activation_derivative(out)
                self.weights[0] += self.learning_rate * (y - out) * self.activation_derivative(out)
                err += 0.5 * (y - out) ** 2
            self.errors.append(err)
        plot.plot(range(len(self.errors)), self.errors)
        plot.savefig(self.name + '_errors.pdf')

    def activation(self, x):
        return x
        #return sigmoid(x)
    def activation_derivative(self, x):
        return 1
        #return sigmoid_derivative(x)
    def output(self, inputs):
        inp = np.concatenate([inputs, fourier_transform(inputs)])
        if self.biased:
            return self.activation(np.dot(self.weights[1:], inp) + self.weights[0])
        else:
            return self.activation(np.dot(self.weights[1:], inp))

class Adaline2(object):
    def __init__(self, no_of_inputs, name = 'name', learning_rate = 0.01, iterations = 500, biased = True):
        self.no_of_inputs = no_of_inputs
        self.learning_rate = learning_rate
        self.iterations = iterations
        self.name = name
        self.biased = biased
        self.weights = np.random.random(self.no_of_inputs + 1) - 0.5
        self.errors = []

    def standarize(self, x):
        return (x - np.mean(x)) / np.std(x)

    def train(self, training_data_x, training_data_y):
        for _ in range(self.iterations):
            err = 0
            for __ in range(len(training_data_x)):
                index = random.randrange(len(training_data_x))
                x = training_data_x[index]
                y = training_data_y[index]
                noised_x = noise(x, 5)
                x_ft = fourier_transform(noised_x)
                out = self.output(noised_x)
                self.weights[1:] += self.learning_rate * (y - out) * x_ft * self.activation_derivative(out)
                self.weights[0] += self.learning_rate * (y - out) * self.activation_derivative(out)
                err += 0.5 * (y - out) ** 2
            self.errors.append(err)
        plot.plot(range(len(self.errors)), self.errors)
        plot.savefig('errors.pdf')

    def activation(self, x):
        return x
        #return sigmoid(x)
    def activation_derivative(self, x):
        return 1
        #return sigmoid_derivative(x)

    def output(self, inputs):
        inp = fourier_transform(inputs)
        if self.biased:
            return self.activation(np.dot(self.weights[1:], inp) + self.weights[0])
        else:
            return self.activation(np.dot(self.weights[1:], inp))

