#!/usr/bin/env python3
import PySimpleGUI as sg
from defines import *
import numpy as np
from PIL import Image, ImageTk
import json
from linear_machine import LinearMachine
import jsonpickle
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--saved', action='store_true', help='load saved weights')
args = parser.parse_args()
saved = args.saved


def to_flat(im):
    arr = np.array(im).flatten()
    return arr


def from_flat(im, size_x, size_y):
    m = np.resize(im, [size_x, size_y])
    return (m * 255).astype(np.uint8)


def magic(machines, vals):
    for i in range(len(vals)):
        res = machines[i].output(vals)
        vals[i] = res


def dist(vals, data):
    dists = []
    for d in data:
        distance = 0
        for i in range(len(vals)):
            distance += (d[i] - vals[i]) % 2
        dists.append(distance)
    return dists


size_y = 50
size_x = 50

layout = []
a = 0
vals = []
total_size = size_x * size_y
tmp = []
tmp.append(sg.Canvas(size=(400, 400), key='canvas', ))
layout.append(tmp)

tmp = [[] for _ in range(7)]
tmp[0].append(sg.Button('clean'))
tmp[0].append(sg.Button('rev'))
tmp[1].append(sg.Button('rand'))
tmp[1].append(sg.Button('print'))
tmp[1].append(sg.Button('remove_noise'))
tmp[1].append(sg.Button('save_machine'))
tmp[2].append(sg.Button('from_training'))
tmp[2].append(sg.InputText(key="from_training_num", default_text=0))
tmp[3].append(sg.Text("", size=(20, 1), key='distance'))
tmp[4].append(sg.Button('edit_position'))
tmp[4].append(sg.InputText(key="x", default_text=0, size=(2, 1)))
tmp[4].append(sg.InputText(key="y", default_text=0, size=(2, 1)))

for t in tmp:
    layout.append(t)

window = sg.Window('gui', layout, margins=(0, 0), element_padding=(0, 0))
window.Finalize()

canvas = window['canvas'].TKCanvas

with open("training_data_zad2.json", "r") as f:
    training_data = json.load(f)
vals = to_flat(training_data[0])
image_data = from_flat(vals, size_x, size_y)
img = ImageTk.PhotoImage(image=Image.fromarray(image_data).resize((400, 400), Image.NEAREST))
canvas.pack()
canvas.create_image(0, 0, anchor="nw", image=img)

machines = [LinearMachine(size_x * size_y, 2) for _ in range(size_x * size_y)]

tdata = []
for j in training_data:
    tdata.append(to_flat(j))
if saved:
    with open("saved_machine_zad2.json", "r") as f:
        string = f.read()
        machines = jsonpickle.decode(string)
else:
    print("train start")
    for i in range(len(machines)):
        machines[i].train_pos(tdata, i)
    print("train stop")

d = dist(vals, tdata)
window['distance'].update(str(d))
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'clean':
        clean(vals)
    elif event == 'rev':
        rev(vals)
    elif event == 'rand':
        rand(vals, 20)
    elif event == 'save_machine':
        saved_machine = jsonpickle.encode(machines)
        with open("saved_machine_zad2.json", "w") as f:
            f.write(saved_machine)
    elif event == 'remove_noise':
        magic(machines, vals)
    elif event == 'print':
        print(from_flat(vals, size_x, size_y) // 255)
    elif event == 'from_training':
        if representsInt(values['from_training_num']):
            i = int(values['from_training_num'])
            if i >= len(training_data) or i < 0:
                print("wrong num")
            else:
                vals = to_flat(training_data[int(values['from_training_num'])])
        else:
            print("not int num")
    elif event == 'edit_position':
        if representsInt(values['x']) and representsInt(values['y']):
            x = int(values['x'])
            y = int(values['y'])
            if x >= size_x or x < 0 or y >= size_y or y < 0:
                print("wrong num")
            else:
                vals[x + y * size_x] = (vals[x + y * size_x] + 1) % 2
        else:
            print("not int num")

    image_data = from_flat(vals, size_x, size_y)
    img = ImageTk.PhotoImage(image=Image.fromarray(image_data).resize((400, 400), Image.NEAREST))
    canvas.pack()
    canvas.create_image(0, 0, anchor="nw", image=img)
    d = dist(vals, tdata)
    window['distance'].update(str(d))
window.close()
