#!/usr/bin/env python3
import json
import random
import numpy as np

def rand(vals, chance = 30):
    tmp = vals.copy()
    amount = (len(tmp) * chance) // 1000
    for i in range(0, amount):
        pos = random.randint(0,len(tmp) - 1)
        tmp[pos] = ( tmp[pos] + 1 ) % 2
    return tmp

class LinearPerceptron(object):
    def __init__(self, no_of_inputs):
        self.no_of_inputs = no_of_inputs
        self.weights = np.random.random(self.no_of_inputs + 1) - 0.5

    def output(self, input):
        summation = np.dot(self.weights[1:], input) + self.weights[0]
        return summation




def argmax(iterable):
    max = iterable[0]
    max_pos = 0
    for pos in range(len(iterable)):
        if iterable[pos] > max:
            max_pos = pos
            max = iterable[pos]
    return max_pos


class LinearMachine(object):
    def __init__(self, no_of_inputs, no_of_categories, learning_rate=0.01, iterations=100):
        self.iterations = iterations
        self.no_of_inputs = no_of_inputs
        self.no_of_categories = no_of_categories
        self.learning_rate = learning_rate
        self.perceptrons = []
        for _ in range(no_of_categories):
            self.perceptrons.append(LinearPerceptron(self.no_of_inputs))

    def train(self, training_data, labels):
        z = list(zip(training_data, labels))
        for _ in range(self.iterations * len(training_data)):
            index = random.randrange(len(training_data))
            data, l = z[index]
            ex = rand(data, 5)
            k = self.output(ex)
            if k != l:
                self.perceptrons[k].weights[1:] -= ex * self.learning_rate
                self.perceptrons[k].weights[0] -= self.learning_rate
                self.perceptrons[int(l)].weights[1:] += ex * self.learning_rate
                self.perceptrons[int(l)].weights[0] += self.learning_rate

    def train_pos(self, training_data, position):
        labels = []
        for i in training_data:
            labels.append(i[position])
        self.train(training_data, labels)

    def output(self, input):
        out = []
        for i in range(self.no_of_categories):
            out.append(self.perceptrons[i].output(input))
        return argmax(out)


