import random


def update(window, vals):
    for i in range(0, len(vals)):
        if vals[i] == 1:
            window[i].update(button_color=("white", "black"))
        else:
            window[i].update(button_color=("black", 'white'))
def representsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
def clean(vals):
    for i in range(0, len(vals)):
        vals[i] = 0
def rev(vals):
    for i in range(0, len(vals)):
        vals[i] = ( vals[i] + 1 ) % 2
def rand(vals, chance = 50):
    for i in range(0, len(vals)):
        if random.randint(1,1000) < chance:
            vals[i] = ( vals[i] + 1 ) % 2


def noise(vals, chance = 30):
    tmp = vals.copy()
    amount = (len(tmp) * chance) // 1000
    for i in range(0, amount):
        pos = random.randint(0,len(tmp) - 1)
        tmp[pos] = ( tmp[pos] + 1 ) % 2
    return tmp


def r(vals, size_x):
    tmp = vals[:]
    for i in range(0, len(vals)):
        y = i // size_x
        x = i % size_x
        new_x = ( x - 1 ) % size_x
        vals[i] = tmp[ y * size_x + new_x ]
def l(vals, size_x):
    tmp = vals[:]
    for i in range(0, len(vals)):
        y = i // size_x
        x = i % size_x
        new_x = ( x + 1 ) % size_x
        vals[i] = tmp[ y * size_x + new_x ]
def up(vals, size_x):
    tmp = vals[:]
    size_y = len(vals) // size_x
    for i in range(0, len(vals)):
        y = i // size_x
        x = i % size_x
        new_y = ( y + 1 ) % size_y
        vals[i] = tmp[ new_y * size_x + x ]
def down(vals, size_x):
    tmp = vals[:]
    size_y = len(vals) // size_x
    for i in range(0, len(vals)):
        y = i // size_x
        x = i % size_x
        new_y = ( y - 1 ) % size_y
        vals[i] = tmp[ new_y * size_x + x ]