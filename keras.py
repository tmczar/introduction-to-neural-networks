#!/usr/bin/env python3
import seaborn as sbs
import matplotlib.pyplot as plt
import numpy as np
import os

if os.path.exists('logs'):
    os.remove('logs')

import tensorflow
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense
import sys
print('Python \\t\\t{0[0]}.{0[1]}'.format(sys.version_info))
print('Tensorflow \\t{}'.format(tensorflow.__version__))
print('Keras \\t\\t{}'.format(keras.__version__))

tensorflow.random.set_seed(2)

inputs = Input(shape=(2,))
hidden1 = Dense(10, activation='relu')(inputs)
hidden2 = Dense(30, activation='relu')(hidden1)
hidden3 = Dense(10, activation='relu')(hidden2)
output = Dense(1, activation='sigmoid')(hidden3)
model = Model(inputs, output)
#print(model.summary())

from sklearn import cluster, datasets

n_samples = 1500
noisy_moons = datasets.make_moons(n_samples=n_samples, noise=.25)

colors_train = np.array(['#377eb8', '#ff7f00'])
colors_test = np.array(['#999999', '#e41a1c', '#dede00'])

#plt.scatter(noisy_moons[0][:, 0], noisy_moons[0][:, 1], s=20, color=colors_train[noisy_moons[1]])
#plt.show()

from sklearn.model_selection import train_test_split

x_train, x_test, y_train, y_test = train_test_split(noisy_moons[0], noisy_moons[1], test_size=0.25)

model.compile(optimizer='adam', loss='mean_squared_error', metrics=[keras.metrics.Accuracy()])
#model.fit(x_train, y_train, epochs=100, batch_size=5, shuffle=True)
#model.evaluate(x_test, y_test)


def plot_decision_boundary(nn_model):
    # Set min and max values and give it some padding
    x_min, x_max = x_train[:, 0].min() - .5, x_train[:, 0].max() + .5
    y_min, y_max = x_train[:, 1].min() - .5, x_train[:, 1].max() + .5
    h = 0.01
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = nn_model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z[Z>=0.5] = 1
    Z[Z<0.5] = 0
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(x_train[:, 0], x_train[:, 1], s=2, c=y_train, cmap=plt.cm.BuGn)

#plot_decision_boundary(model)

from tensorflow.keras.datasets import mnist

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()

img = train_images[0]
#plt.imshow(img)
#plt.show()

train_images = train_images.reshape(60000, 28*28).astype('float32')/255
test_images = test_images.reshape(10000, 28*28).astype('float32')/255

inputs = Input(shape=(784,))
hidden1 = Dense(64, activation='relu')(inputs)
hidden2 = Dense(64, activation='relu')(hidden1)
output = Dense(10, activation='softmax')(hidden2)
model = Model(inputs, output)

model.compile(optimizer=keras.optimizers.RMSprop(), loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics=[keras.metrics.Accuracy()])
model.fit(train_images, train_labels, epochs=10, batch_size=64, validation_data=(test_images, test_labels))
#model.evaluate(x_test, y_test)

from sklearn.metrics import confusion_matrix

exit(0)

fig = plt.figure()
plt.clf()
ax = fig.add_subplot(111)
ax.set_aspect(1)
plt.imshow(cm)
width, height = cm.shape
for x in range(width):
    for y in range(height):
        ax.annotate(str(int(cm[x][y])), xy=(y, x), horizontalalignment='center', verticalalignment='center')

plt.ylabel('True label')
plt.xlabel('Predicted label')
plt.show()

from sklearn.metrics import roc_curve
from sklearn.metrics import auc

fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_true, y_pred)
auc_keras = auc(fpr_keras, tpr_keras)

plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(fpr_keras, tpr_keras, label='Keras (area = {:.3f})'.format(auc_keras))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')
plt.legend(loc='best')
plt.show()
