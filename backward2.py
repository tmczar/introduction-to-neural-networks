import numpy as np


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Example(object):
    def __init__(self, arm_length=1.0):
        self.arm_length = arm_length
        self.center = Point(0, 0)
        self.input = []
        self.output = []

    def generate(self, no_of_examples):
        for i in range(no_of_examples):
            point = self.generate_point()
            self.input.append([point.x/4, point.y/4])
        return self.input

    def generate_point(self):
        alpha = np.random.random() * np.pi
        beta = np.random.random() * np.pi
        self.output.append([alpha / 4, beta / 4])
        temp_point = self.translate(self.center, alpha)
        final_point = self.translate(temp_point, np.pi - beta + alpha)
        # final_point = self.translate(temp_point, beta)
        # final_point = self.translate(temp_point, beta + alpha - np.pi)
        return final_point

    def toPoints(self, res):
        alpha = res[0] * 4
        beta = res[1] * 4
        first = self.translate(self.center, alpha)
        last = self.translate(first, np.pi - beta + alpha)
        return (first, last)

    def translate(self, center, angle):
        return Point(center.x + self.arm_length * np.sin(angle), center.y - self.arm_length * np.cos(angle))


X = np.array(([2, 9], [1, 5], [3, 6]), dtype=float)
y = np.array(([92, 3], [86, 3], [89, 3]), dtype=float)

# scale units
X = X / np.amax(X, axis=0)  # maximum of X array
y = y / 100  # max test score is 100


class Neural_Network(object):
    def __init__(self):
        # parameters
        self.inputSize = 2
        self.outputSize = 2
        self.hiddenSize = 3

        # weights
        self.W1 = np.random.randn(self.inputSize, self.hiddenSize)  # (3x2) weight matrix from input to hidden layer
        self.W2 = np.random.randn(self.hiddenSize, self.outputSize)  # (3x1) weight matrix from hidden to output layer

    def forward(self, X):
        # forward propagation through our network
        self.z = np.dot(X, self.W1)  # dot product of X (input) and first set of 3x2 weights
        self.z2 = self.sigmoid(self.z)  # activation function
        self.z3 = np.dot(self.z2, self.W2)  # dot product of hidden layer (z2) and second set of 3x1 weights
        o = self.sigmoid(self.z3)  # final activation function
        return o

    def sigmoid(self, s):
        # activation function
        return 1 / (1 + np.exp(-s))

    def sigmoidPrime(self, s):
        # derivative of sigmoid
        return s * (1 - s)

    def backward(self, X, y, o):
        # backward propgate through the network
        self.o_error = y - o  # error in output
        self.o_delta = self.o_error * self.sigmoidPrime(o)  # applying derivative of sigmoid to error

        self.z2_error = self.o_delta.dot(
            self.W2.T)  # z2 error: how much our hidden layer weights contributed to output error
        self.z2_delta = self.z2_error * self.sigmoidPrime(self.z2)  # applying derivative of sigmoid to z2 error

        self.W1 += X.T.dot(self.z2_delta)  # adjusting first set (input --> hidden) weights
        self.W2 += self.z2.T.dot(self.o_delta)  # adjusting second set (hidden --> output) weights

    def train(self, X, y):
        o = self.forward(X)
        self.backward(X, y, o)


NN = Neural_Network()
example = Example()
example.generate(10000)
# print(example.input)
# print(example.output)
for _ in range(100):
    NN.train(np.array(example.input), np.array(example.output))
res = NN.forward(example.input[0])
pts = example.toPoints(res)
coords = [pts[0].x, pts[0].y, pts[1].x, pts[1].y]
print(res, coords)
print(example.output[0], example.input[0])
# for i in range(1000): # trains the NN 1,000 times
#    print("Input: \n" + str(X) )
#    print("Actual Output: \n" + str(y) )
#    print("Predicted Output: \n" + str(NN.forward(X)) )
#    print("Loss: \n" + str(np.mean(np.square(y - NN.forward(X))))) # mean sum squared loss
#    print("\n")
#    NN.train(X, y)
