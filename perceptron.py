import numpy as np
import random
#SPLA
def rand(vals):
    tmp = vals.copy()
    for i in range(0, len(tmp)):
        if random.randint(1,100) < 3:
            tmp[i] = ( tmp[i] + 1 ) % 2
    return tmp

class Perceptron(object):

    def __init__(self, no_of_inputs, value, iterations=200, learning_rate=0.01):
        self.iterations = iterations
        self.learning_rate = learning_rate
        self.weights = np.zeros(no_of_inputs + 1)
        for i in range(0, len(self.weights)):
                self.weights[i] = random.uniform(-0.2, 0.2)
        self.weights = np.array(self.weights)
        self.value = value

    def predict(self, inputs):
        summation = np.dot(inputs, self.weights[1:]) + self.weights[0]
        if summation > 0:
            activation = 1
        else:
            activation = 0            
        return activation

    def update_weights(self, prediction, label, data):
        self.weights[1:] += self.learning_rate * (label - prediction) * data
        self.weights[0] += self.learning_rate * (label - prediction)

    def train(self, training_data, labels):
        l_time = 0
        acc = 0
        current_best = 0
        best_acc = 0
        pocket = self.weights
        for epoch in range(self.iterations * len(training_data)):
            index = random.randrange(len(training_data))
            prediction = self.predict(rand(training_data[index]))
            if labels[index] == self.value:
                expect = 1
            else:
                expect = 0
            if expect - prediction == 0:
                l_time += 1
                for data, label in zip(training_data, labels):
                    if label == self.value:
                        expected = 1
                    else:
                        expected = 0
                    if expected - self.predict(data) == 0:
                        acc += 1

                if acc > best_acc and l_time > current_best:
                    best_acc = acc
                    current_best = l_time
                    pocket = self.weights

            else:
                self.update_weights(prediction, expect, training_data[index])
                acc = 0
                l_time = 0
        self.weights = pocket

    def old_train(self, training_inputs, labels):
        for _ in range(self.iterations):
            tmp = zip(training_inputs, labels)
            for inputs, label in tmp:
                tmp_input = rand(inputs)
                if label == self.value:
                    expected = 1
                else:
                    expected = 0
                prediction = self.predict(tmp_input)
                self.update_weights(self, prediction, expected, tmp_input)

    def toJson(self):
        res = {}
        res['value'] = self.value
        res['learning_rate'] = self.learning_rate
        res['weights'] = []
        for weight in self.weights:
            res['weights'].append(weight.tolist())
        res['iterations'] = self.iterations
        return res
