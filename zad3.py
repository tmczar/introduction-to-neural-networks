#!/usr/bin/env python3
import PySimpleGUI as sg
from adaline import Adaline
from adaline import Adaline2
from defines import *
import argparse
import jsonpickle
import json
import numpy as np
from pprint import pprint

parser = argparse.ArgumentParser()
parser.add_argument('--saved', action='store_true',help='load saved weights')
parser.add_argument('--unbiased', action='store_true',help='no bias')
args = parser.parse_args()
saved = args.saved
bias = not args.unbiased


def to_flat(im):
    arr = np.array(im).flatten()
    return arr


def from_flat(im, _size_x, _size_y):
    m = np.resize(im, [_size_x, _size_y])
    return (m * 255).astype(np.uint8)


def magic(machines, vals):
    for i in range(len(vals)):
        res = machines[i].output(vals)
        vals[i] = res

def from_training(num, training_data):
    if num >= len(training_data) or num < 0:
        print("bad number")
    else:
        return training_data[num]['data'].copy()

size_y = 7
size_x = 7

adalines = [Adaline2(size_x * size_y, str(i), biased=bias) for i in range(10)]

with open("training_data_zad3.json", "r") as f:
    training_data = json.load(f)

if saved:
    with open("saved_machine_zad3.json", "r") as f:
        string = f.read()
        adalines = jsonpickle.decode(string)
else:
    print("train start")
    for i in range(len(adalines)):
        train_x = []
        train_y = []
        for j in training_data:
            label = j['label']
            data = j['data']
            train_x.append(data)
            if label == i:
                train_y.append(1)
            else:
                train_y.append(-1)
        adalines[i].train(train_x, train_y)
    print("train stop")

layout = []

a = 0
vals = []
total_size = size_x * size_y
for i in range(0,size_y):
    tmp = []
    for j in range(0,size_x):
        tmp.append(sg.Button('',key = a, button_color=("black", "white"), size=(2, 2)))
        vals.append(0)
        a += 1
    layout.append(tmp)

tmp = [[] for _ in range(6)]
tmp[0].append(sg.Button('clean'))
tmp[0].append(sg.Button('rev'))
tmp[0].append(sg.Button('r'))
tmp[0].append(sg.Button('l'))
tmp[1].append(sg.Button('up'))
tmp[1].append(sg.Button('down'))
tmp[1].append(sg.Button('rand'))
tmp[2].append(sg.Button('save_weights'))
tmp[3].append(sg.Button('add_to_training_data'))
tmp[3].append(sg.InputText(key = "add_label",default_text=0, size=(5, 1)))
tmp[4].append(sg.Text("", size=(10,1), key = 'prediction'))
tmp[4].append(sg.Button('retrain'))
tmp[5].append(sg.Button('from_training'))
tmp[5].append(sg.InputText(key = "from_training_num",default_text=0, size=(5, 1)))

for t in tmp:
    layout.append(t)

window = sg.Window('gui', layout, margins=(0,0), element_padding=(0,0))

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'clean':
        clean(vals)
    elif event == 'rev':
        rev(vals)
    elif event == 'r':
        r(vals, size_x)
    elif event == 'l':
        l(vals, size_x)
    elif event == 'up':
        up(vals, size_x)
    elif event == 'down':
        down(vals, size_x)
    elif event == 'rand':
        rand(vals)
    elif event == 'save_weights':
        saved_machine = jsonpickle.encode(adalines)
        with open("saved_machine_zad3.json", "w") as f:
            f.write(saved_machine)
    elif event == 'add_to_training_data':
        lab = int(values['add_label'])
        tmp = {}
        tmp['label'] = lab
        tmp['data'] = vals.copy()
        training_data.append(tmp)
        with open("training_data_zad3.json", "w") as f:
            f.write(json.dumps(training_data, indent=2))
    elif event == 'from_training':
        if representsInt(values['from_training_num']):
            vals = from_training(int(values['from_training_num']), training_data)
        else:
            print("not int num")
    else:
        o = ( vals[int(event)] + 1 ) % 2
        vals[int(event)] = o
    prediction = []
    for p in adalines:
        prediction.append(p.output(vals))
    pos = 0
    pprint(prediction)
    for i in range(10):
        if prediction[i] > prediction[pos]:
            pos = i
    #window['prediction'].update(str(pos) + ': ' + str(prediction[pos]))
    window['prediction'].update(str(pos))
    update(window, vals)
window.close()
