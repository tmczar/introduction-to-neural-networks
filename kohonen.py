#!/usr/bin/env python3
import numpy as np

class Kohonen(object):

    def __init__(self, h, w, dim):
        self.shape = (h, w, dim)
        self.som = np.random.random(self.shape) - 0.5
        self.data = []
        self.alpha0 = 0.1
        self.sigma = 0.1
        self.beta = 0.1
        self.steps = 300

    def train(self, data):
        self.data = data.copy()
        for t in range(self.steps):
            idx = np.random.choice(range(len(self.data)))
            best_neuron = self.find_best_neuron(self.data[idx])
            self.update_som(best_neuron, self.data[idx], t)

    def find_best_neuron(self, input_vector):
        list_neurons = []
        for x in range(self.shape[0]):
            for y in range(self.shape[1]):
                dist = np.linalg.norm(input_vector - self.som[x, y])
                list_neurons.append(((x, y), dist))
        list_neurons.sort(key=lambda x: x[1])
        return list_neurons[0][0]

    def update_som(self, best_neuron, data_point, t):
        for x in range(self.shape[0]):
            for y in range(self.shape[1]):
                dist_to_best_neuron = np.linalg.norm(np.array(best_neuron) - np.array(self.som[x, y]))
                self.update_cell((x,y), data_point, t, dist_to_best_neuron)

    def update_cell(self, cell, data_point, t, dist_to_best_neuron):
        self.som[cell] += self.alpha(t) + self.G(dist_to_best_neuron) * (data_point - self.som[cell])

    def alpha(self, t):
        return self.alpha0 * np.exp(-t/self.beta)

    def G(self, rho):
        return np.exp(-rho**2/(2*self.sigma**2))

k = Kohonen(20, 20, 2)

training_data = []

k.train(training_data)

