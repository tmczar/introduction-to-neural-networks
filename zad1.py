#!/usr/bin/env python3
import PySimpleGUI as sg
import numpy as np
import random
import json
from perceptron import Perceptron
from pprint import pprint
from matplotlib import pyplot
import argparse
from defines import *

parser = argparse.ArgumentParser()
parser.add_argument('--saved', action='store_true',help='load saved weights')
args = parser.parse_args()
saved = args.saved
def serialize_weights(perceptrions):
    res = []
    for p in perceptrons:
        res.append(p.toJson())
    with open("serialized_weights_zad1.json",'w') as f:
        f.write(json.dumps(res, indent=2))
def print_weights(perceptrons):
    res = []
    for p in perceptrons:
        res.append(p.toJson())
        pprint(json.dumps(res))
def retrain(training_data, size_x, size_y):
    perceptrons = []
    for i in range(10):
        perceptrons.append(Perceptron(size_x * size_y,i))
    labels = []
    data = []
    for example in training_data:
        labels.append(example['label'])
        data.append(np.array(example['data']))

    for perceptron in perceptrons:
        perceptron.train(data, labels)
    return perceptrons
def imshow(perceptrons, num, size_x, size_y):
    if num >= len(perceptrons) or num < 0:
        print("bad number")
    else:
        tmp = perceptrons[num].weights.copy()
        tmp.resize(size_x, size_y)
        pyplot.imshow(np.real(tmp))
        pyplot.show()
def from_training(num, training_data):
    if num >= len(training_data) or num < 0:
        print("bad number")
    else:
        return training_data[num]['data'].copy()

size_y = 5
size_x = 5
perceptrons = []
with open("training_data_zad1.json", "r") as f:
    training_data = json.load(f)
if saved:
    with open("serialized_weights_zad1.json", "r") as f:
        perc_data = json.load(f)
    for p in perc_data:
        per = Perceptron(size_x * size_y, p['value'], iterations = p['iterations'], learning_rate = p['learning_rate'])
        per.weights = np.array(p['weights'])
        perceptrons.append(per)
else:
    perceptrons = retrain(training_data, size_x, size_y)

layout = []

a = 0
vals = []
total_size = size_x * size_y
for i in range(0,size_y):
    tmp = []
    for j in range(0,size_x):
        tmp.append(sg.Button('',key = a, button_color=("black", "white"), size=(2, 2)))
        vals.append(0)
        a += 1
    layout.append(tmp)

tmp = [[] for _ in range(7)]
tmp[0].append(sg.Button('clean'))
tmp[0].append(sg.Button('rev'))
tmp[0].append(sg.Button('r'))
tmp[0].append(sg.Button('l'))
tmp[1].append(sg.Button('up'))
tmp[1].append(sg.Button('down'))
tmp[1].append(sg.Button('rand'))
tmp[1].append(sg.Button('print'))
tmp[2].append(sg.Button('print_weights'))
tmp[2].append(sg.Button('save_weights'))
tmp[3].append(sg.Button('add_to_training_data'))
tmp[3].append(sg.InputText(key = "add_label",default_text=0, size=(5,1)))
tmp[4].append(sg.Text("", size=(10,1), key = 'prediction'))
tmp[4].append(sg.Button('retrain'))
tmp[5].append(sg.Button('imshow'))
tmp[5].append(sg.InputText(key = "imshow_num",default_text=0, size=(5,1)))
tmp[6].append(sg.Button('from_training'))
tmp[6].append(sg.InputText(key = "from_training_num",default_text=0, size=(5,1)))

for t in tmp:
    layout.append(t)

window = sg.Window('gui', layout, margins=(0,0), element_padding=(0,0))

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'clean':
        clean(vals)
    elif event == 'rev':
        rev(vals)
    elif event == 'r':
        r(vals, size_x)
    elif event == 'l':
        l(vals, size_x)
    elif event == 'up':
        up(vals, size_x)
    elif event == 'down':
        down(vals, size_x)
    elif event == 'rand':
        rand(vals)
    elif event == 'print':
        print(vals)
    elif event == 'print_weights':
        print_weights(perceptrons)
    elif event == 'save_weights':
        serialize_weights(perceptrons)
    elif event == 'retrain':
        perceptrons = retrain(training_data, size_x, size_y)
    elif event == 'add_to_training_data':
        lab = int(values['add_label'])
        tmp = {}
        tmp['label'] = lab
        tmp['data'] = vals.copy()
        training_data.append(tmp)
        with open("training_data_zad1.json", "w") as f:
            f.write(json.dumps(training_data, indent=2))
    elif event == 'imshow':
        if representsInt(values['imshow_num']):
            imshow(perceptrons,int(values['imshow_num']), size_x, size_y)
        else:
            print("not int num")
    elif event == 'from_training':
        if representsInt(values['from_training_num']):
            vals = from_training(int(values['from_training_num']), training_data)
        else:
            print("not int num")
    else:
        o = ( vals[int(event)] + 1 ) % 2
        vals[int(event)] = o
    prediction = []
    for p in perceptrons:
        prediction.append(p.predict(vals))
    pp = []
    for i in range(10):
        if prediction[i]:
            pp.append(i)
    window['prediction'].update(str(pp))
    update(window,vals)
window.close()

